.var chrout = $ffd2   // kernal addresss 
.var screen	= $1e00
.var colors	= $9600

.var low = $900a
.var mid = $900b
.var high	= $900c
.var noise	= $900d
.var volume	= $900e

.var pattern_pointer = $59
.var channel_pointer = $28	// product area for multiplication
.var workarea 		= $57

//   2014 SYS 4110 
        .pc=$fff  // unexpanded                         
		.byte $01, $10
        .byte $0c, $10, $dE, $07
        .byte $9e, $20, $34, $31, $31, $30 
        .byte $00, $00, $00
lab4110: jmp main         // after basic start line 
                  // machine code starts at $100e = #4110 

main:    
		lda #$7f
		sta $912e     // disable and acknowledge interrupts
		sta $912d
		sta $911e     // disable NMIs (Restore key)

		lda		#$01	// bg colors and stuff
		sta		$900f
		
		// if draw_graphics is off, we don't touch the screen RAM
		lda		draw_graphics
		cmp		#1
		bne		exit_title_loop
		jsr		clear
		
		lda		#0
		sta		$00D1	// reset the line
		
		lda		#0
loop:	cmp  title_length
		beq exit_title_loop
		pha
		tax
		lda		title, x
		//lda #$02		
		sta		screen, x
		lda		#01
		sta		colors, x	// background color for text
		
		// increase a
		pla
		tax
		inx
		txa
		jmp loop
		
exit_title_loop: ldx #0
		jsr		setup_music
		jmp		skip_order_wrap // enter in the middle of the music loop
		//jmp music
		
halt:	jmp		halt
		
		
setup_music: 	lda		#$08
				sta		volume
				rts
		
// main pattern loop
music:	ldx #0	
			
patloop: stx	row

		txa
		pha	// push x
		
		// at the beginning we point at the first channel
		jsr reset_channel_pointer
		
		ldx #0
channel_loop: stx $26	// we save the channel index to zero page
		txa
		pha	// push x
		
		clc
		lda channel_pointer
		adc row	// row is just 8 bits long variable
		sta workarea
		lda channel_pointer + 1
		adc #0	// just apply the carry flag if needed
		sta workarea + 1
		
		// row pointer now lies at workarea - workarea+1
		ldx #0
		lda (workarea, x)
		ldx $26 // load the channel index to X
		
		// do nothing if note is 0x00
		cmp #$00		
		beq skip_channel_update
		// 0x01 is notecut
		cmp #$01
		bne skip_notecut
		lda #0
		sta low, x
		jmp end_effects
skip_notecut: cmp #16
		//jmp end_effects
		// branch if less than 15
		bcc effect_speed
		clc
		// branch if more than 32
		cmp #32
		beq here
		bcs effect_speed
here:	clc
		sbc #15
		sta volume // overwrite auxiliary color information like a boss
		//sta screen+6
		jmp skip_channel_update
		
effect_speed:	cmp #$20
	bcc end_effects
	clc
	// branch if more than 64
	cmp #$40
	beq here2
	bcs end_effects	
here2: clc	
	sbc #$20
	asl // multiply by eight to scale speed from 0-31 to 0-255
	asl
	asl
	sta ticklength
	
	jmp skip_channel_update
	
		
end_effects:	sta low, x	

		// visualization
		//sta $900f - 1, x	
		
skip_channel_update:	clc
		
		jsr increase_channel_pointer
		
		pla
		tax	// pull x (channel loop)
		inx
		txa
		cmp #4
		bne channel_loop
	
		jsr debugprint
		
		pla
		tax	// pull x
		
		jsr delay
		inx
		cpx #64	
		bne patloop
		
		
		// if the current order is over order_amount, then loop
		ldx order
		inx
		cpx order_amount
		bne skip_order_wrap
		ldx #0
skip_order_wrap:	stx order
				
		// load the correct pattern index
		lda orderlist, x
		sta pattern
		
		// store pattern0 pointer in zeropage
		ldx #<pattern0
		stx workarea
		ldx #>pattern0
		stx workarea + 1
		
		jsr reset_pattern_pointer
		
		
		ldx #0
mult_loop:	cpx pattern	// pattern index
		beq leave_mult

		inx
		
		lda workarea
		// no addition needs to be done since 0x0100 lower byte is 0x00
		sta pattern_pointer // less significant output byte
		lda	workarea + 1
		clc
		adc #1
		sta pattern_pointer + 1 // more significant output byte
		sta workarea + 1
		
		jmp mult_loop
leave_mult: jmp music // } end_while 
		
		
delay:	pha // push a
		txa
		pha // push x
		
		ldx #0
delayloop2: inx
		txa
		pha	// push x
		
		ldx #0
delayloop:	inx
		nop
		nop
		cpx #255
		bne	delayloop
		
		pla // pull x
		tax
		cpx ticklength
		bne delayloop2
		
		pla // pull x
		tax
		pla // pull a
		rts
		
debugprint: pha

		// skip draw  if draw_graphics is 0
		lda		draw_graphics
		cmp		#1
		bne		no_draw
		
		lda		order
		sta		screen + 512 - 26 + 0
		lda		pattern
		sta		screen + 512 - 26 + 1
		lda		row
		sta		screen + 512 - 26 + 2
no_draw:	pla
		rts

// stores pattern0 pointer in zeropage at pattern_pointer
reset_pattern_pointer: ldx #<pattern0
		stx pattern_pointer		
		ldx #>pattern0
		stx pattern_pointer + 1
		rts
		
reset_channel_pointer: lda pattern_pointer
		sta channel_pointer
		lda pattern_pointer + 1
		sta channel_pointer + 1		
		rts

// calculate row pointer by adding offset to pattern_pointer
// save the offsetted value as channel_pointer
increase_channel_pointer:	lda channel_pointer
		adc #64	
		sta channel_pointer
		lda channel_pointer + 1
		adc #0	// just apply the carry flag if needed
		sta channel_pointer + 1

		rts
		
		// http://sta.c64.org/cbm64pet.html
		// 95 on kolmio, 105 on toiseen suuntaan
		// 102 on 1x1 dither
		// #32 on space
clear:	ldx		#0	
clear2:	lda		#32
		sta		screen, x
		sta		screen + $100, x
		
		// color
		lda		#$01
		sta		$9600, x		// upper half of the screen
		sta		$9600 + $100, x // lower half
		
		inx
		cpx		#00
		bne		clear2
		rts


// player variables
order:	.byte	0
pattern:	.byte	0
row:		.byte	0
//ticklength	.equ	64

// song data
.import source "tune.s"
