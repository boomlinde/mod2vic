#!/usr/bin/python
# At least python 3.3 required

import argparse
import string
from lib.modtag.modtag import load_module


parser = argparse.ArgumentParser(description='do vic magick')
parser.add_argument('module', help='The module where to load data from')
parser.add_argument('--output', help='Where to save the assembly source', default='tune.s')
parser.add_argument('--tempo', help='song tempo (0-255) bigger is slower', default='32', type=int)
parser.add_argument('--title', help='song title displayed on screen', default='')
parser.add_argument('--draw_graphics', help='show text and playback information on screen in the resulting binary', default='1', type=int)
parser.add_argument('--verbose', action='store_true')

args = parser.parse_args()

valid_chars = "-_.() %s%s" % (string.ascii_letters, string.digits)
EMPTY_NOTE_SEMITONE = -999
CODE_NOTEOFF = 0x01
CODE_VOLUME = 0x10 # these actually range from 0x10 to 0x20
CODE_SPEED = 0x20 # from 0x20 to 0x40
CODE_JUMP = 0x40 # from 0x40 to 0x50

# VIC-20 o/
def mapNoteToVIC(semitones):
	"""
	semitones	the note pitch as semitone offset from the lowest C
				e.g. C0 = 0, F#0 = 6
	"""

	if semitones < 0 or semitones > 38:
		raise Exception("Invalid semitone %d" % (semitones))

	tones = [
		135, 143, 147, 151, 159, 163,		
		167, 175, 179, 183, 187, 191,		
		195, 199, 201, 203, 207, 209,		
		212, 215, 217, 219, 221, 223,		
		225, 227, 228, 229, 231, 232,		
		233, 235, 236, 237, 238, 239,		
		240, 241
	]

	return tones[semitones]


def period_to_semitone(period):
	"""Amiga period to semitone"""
	table =[856,808,762,720,678,640,604,570,538,508,480,453,
			428,404,381,360,339,320,302,285,269,254,240,226,
			214,202,190,180,170,160,151,143,135,127,120,113]

	for i in range(0, 3*12):
		if table[i]==period:
			semitone_offset = i - 12 # root note is C-4
			return semitone_offset

	return EMPTY_NOTE_SEMITONE



f = open(args.module, "rb")
songbytes = f.read()
f.close()

def serialize_orderlist(orderlist):
	ordernames = map(str, orderlist)
	bytelist = ', '.join(ordernames)

	return "orderlist:\t.byte\t%s\n" % (bytelist)

mod = load_module(songbytes, {'verbose': args.verbose})

print("orderlist: %s" % (serialize_orderlist(mod.orderlist)))


def find_note_delta(channel, index):
	""" calculates the amount of notes until the next note """
	amount = 0

	for i in range(index+1, 64):
		note = channel[i]

		if note.pitch != 0:
			return amount

		amount += 1

	return amount

def compress_pattern(pat):
	out = []
	# 4 channels
	out.append([]) 
	out.append([])
	out.append([])
	out.append([])

	# pat.rows[1] = contains a list of channel #2 notes
	for chan_index, channel in enumerate(pat.rows):
		#print ("channel %s:" % (chan_index))
		#print ("\t%d notes" % (len(channel)))

		out[chan_index] = []

		for note_index, note in enumerate(channel):
			# 0x0E == note cut effect
			if note.pitch == 0 and note.effect != 0x0E:
				continue

			notedata = (note_index, find_note_delta(channel, note_index), note)
			out[chan_index].append(notedata)
			#print("%d" % (find_note_delta(channel, note_index)))
			#print("\t%d: %d %d" % (note_index, period_to_semitone(note.pitch), (find_note_delta(channel, note_index))) )

	return out

# converts to uppercase petscii
def serialize_string(string):
	if len(string) == 0:
		return ("title:\t.byte\t0", 0)

	chars = 0
	petscii = []

	for c in string:
		asc = ord(c.lower())
		char = ""

		if asc >= 97 and asc <= 122:
			char = "%d" % (asc-97 + 1)
			chars+=1

		if asc == 32:
			char = "32"
			chars+=1

		petscii.append(char)

	return ('title:\t.byte\t' + ', '.join(petscii), chars)


petscii_title, title_length = serialize_string(args.title)
print ("song title: %s" % (args.title))

outstring = ""
outstring += "order_amount:\t.byte\t%d\n" % (len(mod.orderlist))
outstring += "pattern_amount:\t.byte\t%d\n" % (mod.num_patterns)
outstring += serialize_orderlist(mod.orderlist)
outstring += "ticklength:\t.byte\t%d\n" % (int(args.tempo))
outstring += petscii_title 
outstring += "\n"
outstring += "title_length:\t.byte\t%d\n" % (title_length)
outstring += "draw_graphics:\t.byte\t%d\n" % (args.draw_graphics)

print("amount of patterns: %d" % (mod.num_patterns))

for i, pat in enumerate(mod.patterns):
	"""
	if i not in mod.orderlist:
		print("pattern %d not used, skipping" % (i))
		continue
	"""

	print ("pattern %d " % (i))
	outstring += "\n"

	#newpat = compress_pattern(pat)

	#compressed_note_amount = sum(map(len, newpat))

	pattern_prelude = "pattern%d:\t.byte " % (i)
	outstring += pattern_prelude
	notes = []
	#print("compressed notes: %d" % (compressed_note_amount))
	for chan_index, chan in enumerate(pat.rows):
		for note_index, note in enumerate(chan):
			""""
			if note.pitch == 0 and note.effect != 0x0E:
				continue
			"""

			if note.effect == 0x0E and note.parameters == 0xC0:
				notes.append("%d" % (CODE_NOTEOFF))
				continue

			# volume effect
			if note.effect == 0x0C:
				truevolume = round((float(note.parameters)/64.0)*16.0)
				#print("truevolume %d -> %d" % (note.parameters, truevolume))
				code = CODE_VOLUME + max(0,min(15, truevolume))
				notes.append("%d" % (code))
				continue

			# speed effect
			if note.effect == 0x0F:
				# only accept values between 0 - 31
				speed = min(31, note.parameters)
				code = CODE_SPEED + speed
				notes.append("%d" % (code))
				continue

			# jump to order effect
			if note.effect == 0x0B:
				if note.parameters > 15:
					raise Exception("Too high order for jump: %d" % (note.parameters))
				code = CODE_JUMP + note.parameters
				notes.append("%d" % (code))
				continue


			semitone = period_to_semitone(note.pitch) 

			if semitone == EMPTY_NOTE_SEMITONE:
				vicperiod = 0
			else:
				try:
					vicperiod = mapNoteToVIC(semitone + 12 ) 
				except Exception as e:
					raise Exception("note.pitch: " + str(note.pitch) +", " + str(e))

			# index, delta, note
			#print("%d:\t%s" % (note_index, vicperiod))

			notes.append("%d" % (vicperiod))

	outstring += ', '.join(notes)
		

#print("out:" + outstring)
outp = open(args.output, "w")
outp.write(outstring)
outp.close()

print ("saved to %s" % (args.output))
